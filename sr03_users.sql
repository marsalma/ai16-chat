-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 14 juin 2022 à 21:30
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sr03_chat`
--

-- --------------------------------------------------------
--
-- Structure de la table `sr03_channels`
--

DROP TABLE IF EXISTS `sr03_channels`;
CREATE TABLE IF NOT EXISTS `sr03_channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `description` varchar(100) COLLATE utf8_bin NOT NULL,
  `owner` int(11) NOT NULL,
  `start_date` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `end_date` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_foreign_key_name` (`owner`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `sr03_channels`
--

INSERT INTO `sr03_channels` (`id`, `name`, `description`, `owner`, `start_date`, `end_date`, `status`) VALUES
(1, 'channel_1', 'blabalabalalalababalablab', 2, NULL, NULL, '0'),
(2, 'channel_2', 'blabalabalalalababalablab', 2, NULL, NULL, '0'),
(3, 'channel_3', 'blabalabalalalababalablab', 2, NULL, NULL, '0'),
(5, 'channel de test', 'blabalabalalalababalablab', 2, NULL, NULL, '0'),
(6, 'bonjour', 'blabalabalalalababalablab', 2, NULL, NULL, '0'),
(7, 'Test de date', 'test ', 4, '2023-01-12T19:30', '2023-01-23T19:30', '1'),
(9, 'deviendra expiré', 'depechez vous', 4, '2021-12-12T19:30', '2022-06-29T18:22', '0'),
(10, 'encore', 'test', 4, '2021-01-12T19:30', '2022-06-12T18:27', '1'),
(11, 'tttttttttttttt', 'gg hhhhh hhhhhh hhhhh', 2, '2022-01-12T19:30', '2022-06-12T18:31', '1');

-- --------------------------------------------------------

--
-- Structure de la table `sr03_channels_users`
--

DROP TABLE IF EXISTS `sr03_channels_users`;
CREATE TABLE IF NOT EXISTS `sr03_channels_users` (
  `users_id` int(11) NOT NULL,
  `channels_id` int(11) NOT NULL,
  PRIMARY KEY (`users_id`,`channels_id`),
  KEY `channels_users_ibfk_2` (`channels_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `sr03_channels_users`
--

INSERT INTO `sr03_channels_users` (`users_id`, `channels_id`) VALUES
(1, 1),
(2, 1),
(4, 1),
(5, 1),
(1, 2),
(2, 2),
(2, 5),
(1, 6),
(4, 6),
(5, 6),
(2, 7);

-- --------------------------------------------------------


--
-- Structure de la table `sr03_users`
--

DROP TABLE IF EXISTS `sr03_users`;
CREATE TABLE IF NOT EXISTS `sr03_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(20) COLLATE utf8_bin NOT NULL,
  `mail` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `failed_attempt` int(11) DEFAULT NULL,
  `lock_time` date DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `sr03_users`
--

INSERT INTO `sr03_users` (`id`, `firstname`, `lastname`, `mail`, `password`, `admin`, `failed_attempt`, `lock_time`, `enabled`, `locked`) VALUES
(1, 'Jean', 'Pierre', 'jpp@mail.com', '$2a$10$5VOrScbEJQTcb3yWrrRCNeeUGq11j6p8K7sac7pRDW43xndAHw/62', 0, 0, NULL, 0, 0),
(2, 'Marie', 'Jeanne', 'mj@mail.com', '$2a$10$1CZutnvNoghGrLfJx1CyGe6pn4fRSCqnrJ9e6zH/J9UgsN2XoA0Q.', 1, 0, NULL, 1, 0),
(4, 'Audrey', 'Combary', 'ac@mail.com', '$2a$10$Bmp35awcQRPTpJZRG5Ikfuwd8pcQHxie9yCxBSE5PWMWdYAk8ryX2', 0, 0, NULL, 1, 0),
(5, 'Marine', 'Marsal', 'mm@gmail.com', '$2a$10$wGgrBHCrm7DvtTno86/2R.Yo8rxiWjpI/HVFms4rbO4M3IKhQ875i', 0, 0, '2022-06-15', 1, 1);
COMMIT;

--Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `sr03_channels`
--
ALTER TABLE `sr03_channels`
  ADD CONSTRAINT `fk_foreign_key_name` FOREIGN KEY (`owner`) REFERENCES `sr03_users` (`id`);

--
-- Contraintes pour la table `sr03_channels_users`
--
ALTER TABLE `sr03_channels_users`
  ADD CONSTRAINT `channels_users_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `sr03_users` (`id`),
  ADD CONSTRAINT `channels_users_ibfk_2` FOREIGN KEY (`channels_id`) REFERENCES `sr03_channels` (`id`);
COMMIT;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
