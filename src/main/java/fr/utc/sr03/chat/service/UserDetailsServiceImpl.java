package fr.utc.sr03.chat.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.utc.sr03.chat.dao.AppUserRepository;
import fr.utc.sr03.chat.details.AppUserDetails;
import fr.utc.sr03.chat.model.AppUser;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
    @Autowired
    private AppUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
        AppUser appUser = this.userRepository.findByMail(mail);

        if (appUser == null) {
            System.out.println("User not found! " + mail);
            throw new UsernameNotFoundException("User " + mail + " was not found in the database");
        }

        System.out.println("Found User: " + appUser);

        // [ROLE_USER, ROLE_ADMIN,..]
        ArrayList<String> roleNames = new ArrayList<>();
        roleNames.add("ROLE_USER");
        if(appUser.isAdmin())
        	roleNames.add("ROLE_ADMIN");
        
      
        System.out.println("Users roles " + roleNames.toString());

        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
        if (roleNames != null) {
            for (String role : roleNames) {
                // ROLE_USER, ROLE_ADMIN,..
                GrantedAuthority authority = new SimpleGrantedAuthority(role);
                grantList.add(authority);
            }
        }
/*
        UserDetails userDetails = (UserDetails) new User(appUser.getMail(), //
                appUser.getPassword(), grantList);
                
                  return userDetails;

*/
       // return new AppUserDetails(appUser);
        
        AppUserDetails appUserDetail=new AppUserDetails(appUser);
        appUserDetail.setAuthorities(grantList);

        return appUserDetail;
    }
         
}