package fr.utc.sr03.chat.snippets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.utc.sr03.chat.model.AppUser;

/*
 * URL du endpoint : http://localhost:8080/snippets/sendmail
 */
@Controller
public class SendMail {
    @Autowired
    private JavaMailSender javaMailSender;

    @GetMapping("sendmail")
    @ResponseBody
    public String sendMail(AppUser user) {
        SimpleMailMessage msg = new SimpleMailMessage();
        String psw =user.getPassword();
        msg.setTo(user.getMail());
        msg.setSubject("Nouveau compte");
        msg.setText("Se connecter à I chat UTchat  http://localhost:8080/signin. Voici le mdp :" + psw);

        javaMailSender.send(msg);

        return "ok";
    }
}