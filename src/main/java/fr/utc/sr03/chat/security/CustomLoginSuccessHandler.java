package fr.utc.sr03.chat.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import fr.utc.sr03.chat.details.AppUserDetails;
import fr.utc.sr03.chat.model.AppUser;
import fr.utc.sr03.chat.service.UserService;
 
@Component
public class CustomLoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
 
    @Autowired
    private UserService userService;
     
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {
        AppUserDetails userDetails =  (AppUserDetails) authentication.getPrincipal();
        AppUser user = userDetails.getUser();
        if (user.getFailedAttempt() > 0) {
            userService.resetFailedAttempts(user.getMail());
        }
         
        super.onAuthenticationSuccess(request, response, authentication);
    }
     
}