package fr.utc.sr03.chat.details;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import fr.utc.sr03.chat.model.AppUser;


public class AppUserDetails implements UserDetails {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private AppUser user;
	
	private List <GrantedAuthority> authorities=null;
     
    
    public AppUserDetails(AppUser user) {
        this.user = user;
    }
 
   /* @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
    	 ArrayList<String> roleNames = new ArrayList<>();
         roleNames.add("ROLE_USER");
         if(user.isAdmin())
         	roleNames.add("ROLE_ADMIN");
         if (roleNames != null) {
             for (String role : roleNames) {
                 // ROLE_USER, ROLE_ADMIN,..
                 GrantedAuthority authority = new SimpleGrantedAuthority(role);
                 grantList.add(authority);
             }
         }
         
         
        return grantList;
    }
 */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }
    @Override
    public String getPassword() {
        return user.getPassword();
    }
 
    @Override
    public String getUsername() {
        return user.getMail();
    }
 
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
 
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
 
    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

	public AppUser getUser() {
		return user;
	}
	public void setUser(AppUser user) {
		this.user=user;
	}

	public void setAuthorities(List<GrantedAuthority> grantList) {
		this.authorities = grantList;
		
	}

	@Override
	public boolean isAccountNonLocked() {
		return !user.isLocked();
	}
}