package fr.utc.sr03.chat.model;

public enum Status {
    OPEN {
        @Override
        public String toString() {
          return "Disponible";
        }
      }, 
    COMMING_SOON {
        @Override
        public String toString() {
          return "A venir";
        }
      }, 
    EXPIRED {
    	    @Override
    	    public String toString() {
    	      return "Expiré";
    	    }
    	  };
}