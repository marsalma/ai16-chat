package fr.utc.sr03.chat.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "sr03_channels")
public class Channel {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY) // strategy=GenerationType.IDENTITY => obligatoire pour auto increment mysql
	private long id;
	private String name;
	private String description;
	@Column(name = "start_date")
	private String startDate;
	@Column(name = "end_date")
	private String endDate;
	
	@Enumerated(EnumType.ORDINAL)
	private Status status;
	
	
	@ManyToOne
    @JoinColumn(name="owner", nullable=false)
	private AppUser owner;
	
	@ManyToMany(mappedBy = "channels")
    Set<AppUser> users = new HashSet<>();
	
	
	public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<AppUser> getUsers() {
		return users;
	}
	
	public void setUsers(Set<AppUser> users) {
		this.users = users;
	}

	public AppUser getOwner() {
		return owner;
	}

	public void setOwner(AppUser owner) {
		this.owner = owner;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Status getStatus() {
		SimpleDateFormat formatter5=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");  
    	Date dateS = null;
    	Date dateE = null;
    	if(this.getStartDate() != null && this.getEndDate() != null) {
    		try {
    			dateS = formatter5.parse(this.getStartDate());
    			dateE=formatter5.parse(this.getEndDate());
    			
    			if(dateS.after(new Date()))
    				status = Status.COMMING_SOON;
    			else if (dateE.before(new Date()))
    				status = Status.EXPIRED;
    			else
    				status = Status.OPEN;
    			
    		} catch (ParseException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			status = Status.OPEN;
    		}	
    	} else {
    		status = Status.OPEN;
    	}

		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
