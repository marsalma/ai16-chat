package fr.utc.sr03.chat.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "sr03_users")
public class AppUser {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY) // strategy=GenerationType.IDENTITY => obligatoire pour auto increment mysql
    private long id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(unique = true)
    private String mail;

    private String password;

    private boolean admin;
    
    @Column(name = "failed_attempt")
    private int failedAttempt;
     
    @Column(name = "lock_time")
    private Date lockTime;
    
    private boolean enabled;
    
    private boolean locked;

	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "sr03_channels_users", 
        joinColumns = { @JoinColumn(name = "users_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "channels_id") }
    )
    Set<Channel> channels = new HashSet<>();
	
	@OneToMany(mappedBy="owner")
	Set<Channel> channelsOwned = new HashSet<>();
	
    public AppUser(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    
    @Override
    public String toString() {
        return this.mail + "/" + this.password;
    }
    
    public String allInfo() {
    	return this.firstName + " " + this.lastName + " " + this.mail + " " + this.password + " " + this.admin;
    }

	public int getFailedAttempt() {
		// TODO Auto-generated method stub
		return failedAttempt;
	}
	
	public void setFailedAttempt(int failedAttempt) {
		// TODO Auto-generated method stub
		this.failedAttempt=failedAttempt;
	}
	
	public void setLockTime(Date lockTime) {
		this.lockTime=lockTime;
	}
	
	public Date getLockTime() {
		return lockTime;
	}
	
	public Set<Channel> getChannels() {
		return channels;
	}
	
	public void setChannels(Set<Channel> channels) {
		this.channels = channels;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public Set<Channel> getChannelsOwned() {
		return channelsOwned;
	}
	
	public void setChannelsOwned(Set<Channel> channelsOwned) {
		this.channelsOwned = channelsOwned;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}
}

