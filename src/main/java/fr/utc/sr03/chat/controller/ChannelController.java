package fr.utc.sr03.chat.controller;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.utc.sr03.chat.dao.AppUserRepository;
import fr.utc.sr03.chat.dao.ChannelRepository;
import fr.utc.sr03.chat.details.AppUserDetails;
import fr.utc.sr03.chat.model.AppUser;
import fr.utc.sr03.chat.model.Channel;
import fr.utc.sr03.chat.model.Status;

@Controller
public class ChannelController {
	
	 @Autowired
	 private AppUserRepository userRepository;
	 @Autowired
	 private ChannelRepository channelRepository;

	@RequestMapping(value = "/channels", method = RequestMethod.GET)
    public String getChannels(@RequestParam(required=false) String error, Model model, Principal principal) {
    	System.out.println("ChannelController, getChannels");
    	if(error != null && !error.equals("False")) {
        	if(error.equals("1"))
        		model.addAttribute("message_erreur", "Vous n'êtes pas propriétaire du channel");
        	if(error.equals("2"))
        		model.addAttribute("message_erreur", "Vous n'êtes ni utilisateur du channel ni propriétaire");
        	if(error.equals("3"))
        		model.addAttribute("message_erreur", "Vous n'êtes pas utilisateur du channel");
    	}
    	
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	AppUser appUser = userRepository.findByMail(loginedUser.getUsername());
    	
        Set<Channel> channels = appUser.getChannels();      
        	
        model.addAttribute("channels", channels);
        
        return "channel_list";
    }
	
	@RequestMapping(value = "/channels-owned", method = RequestMethod.GET)
    public String getChannelsOwned(@RequestParam(required=false) String error, Model model, Principal principal) {
    	System.out.println("ChannelController, getChannelsOwned");
    	if(error != null && !error.equals("False")) {
        	if(error.equals("1"))
        		model.addAttribute("message_erreur", "Vous n'êtes pas propriétaire du channel");
        	if(error.equals("2"))
        		model.addAttribute("message_erreur", "Vous n'êtes ni utilisateur du channel ni propriétaire");
        	if(error.equals("3"))
        		model.addAttribute("message_erreur", "Vous n'êtes pas utilisateur du channel");
    	}
    	
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	AppUser appUser = userRepository.findByMail(loginedUser.getUsername());
    	     
        Set<Channel> channelsOwned = appUser.getChannelsOwned();
        	
        model.addAttribute("channels_owned", channelsOwned);
        
        return "channel_owned_list";
    }
    
    @RequestMapping(value = "/channel/{id}", method = RequestMethod.GET)
    public String connectToChannel1(@PathVariable("id") long id, Model model, Principal principal) {
    	System.out.println( "on affiche le chat "+id);
    	
    	Channel channel = channelRepository.getById(id);
    	
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	AppUser us = userRepository.findByMail(loginedUser.getUsername());
	   	if(!us.getChannels().contains(channel) && channel.getOwner().getId() != us.getId())
    		return "redirect:/channels?error=2";
    	
    	model.addAttribute("id", channel.getId());
    	model.addAttribute("name", channel.getName());
    	model.addAttribute("date_end", channel.getEndDate());
    	
    	System.out.println("ChannelController connectToChannel1 username " + loginedUser.getUsername() );
    	model.addAttribute("username", loginedUser.getUsername());
    	
        return "channel";
    }
    
    @RequestMapping(value = "/channel/create_channel", method = RequestMethod.GET)
    public String getCreateChannel(Model model, @RequestParam(required=false) String error) {
    	if(error != null && !error.equals("False")) {
        	if(error.equals("1"))
        		model.addAttribute("message_erreur", "La date de fin est antérieure à la date de début");
        	if(error.equals("2"))
        		model.addAttribute("message_erreur", "La date de fin est antérieure à la date actuelle");
        	if(error.equals("3"))
        		model.addAttribute("message_erreur", "Erreur dans la saisie des dates");
    	}
    	model.addAttribute("channel", new Channel());
    	return "create_channel";
    }
    
    @RequestMapping(value = "/channel/create_channel", method = RequestMethod.POST)
    public String createChannel(@ModelAttribute Channel channel,  Model model, Principal principal) {
    	
    	SimpleDateFormat formatter5=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");  
    	Date dateS = null;
    	Date dateE = null;
		try {
			dateS = formatter5.parse(channel.getStartDate());
			dateE=formatter5.parse(channel.getEndDate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "redirect:/channel/create_channel?error=3";
		}
        	
    	System.out.println("Date début "+ dateS);
    	System.out.println("Date de fin "+ dateE);
    	
    	if(dateS.after(dateE))
    		return "redirect:/channel/create_channel?error=1";
    	
    	if(dateE.before(new Date()))
    		return "redirect:/channel/create_channel?error=2";
    	
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	
    	AppUser appUser = userRepository.findByMail(loginedUser.getUsername());
    	channel.setOwner(appUser);
    	
    	channel.setStatus(Status.COMMING_SOON);
    	channelRepository.save(channel);
    	
        return "redirect:/channels-owned";
    }
    
    @RequestMapping(value = "/channel/{id}/users", method = RequestMethod.GET)
    public String getUtilisateursChannel(@PathVariable("id") long id, Model model, Principal principal) {
    	
    	Channel channel = channelRepository.getById(id);
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	AppUser us = userRepository.findByMail(loginedUser.getUsername());
	   	if(channel.getOwner().getId() != us.getId())
    		return "redirect:/channels-owned?error=1";
	   	
    	model.addAttribute("users", channel.getUsers()); // liste des utilisateurs du channel
    	
    	List<AppUser> users = userRepository.findUsersNotInChannel(id);
    	users.remove(channel.getOwner());
    	model.addAttribute("listeUsers", users); // liste des utilisateurs à ajouter 
    	
    	model.addAttribute("channel_id", id);
    	
    	model.addAttribute("selected_user", new AppUser()); // utilisateur vide 
    	
    	return "channel_users";
    }
    
    @RequestMapping(value = "/channel/{id}/delete", method = RequestMethod.GET)
    public String deleteChannel(@PathVariable("id") long id, Principal principal) {
    	
    	Channel channel = channelRepository.getById(id);
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	AppUser us = userRepository.findByMail(loginedUser.getUsername());
	   	if(channel.getOwner().getId() != us.getId())
    		return "redirect:/channels-owned?error=1";
	   	
    	Set<AppUser> users = channel.getUsers();
    	for(AppUser user : users) {
    		user.getChannels().remove(channel);
    		userRepository.save(user);
    	}
    	channelRepository.delete(channel);
    	return "redirect:/channels-owned";
    }
    
    @RequestMapping(value = "/channel/{id}/leave", method = RequestMethod.GET)
    public String leaveChannel(@PathVariable("id") long id, Model model, Principal principal) {
    	
    	Channel channel = channelRepository.getById(id);
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	
    	AppUser appUser = userRepository.findByMail(loginedUser.getUsername());
    	appUser.getChannels().remove(channel);
    	userRepository.save(appUser);
    	
    	return "redirect:/channels";
    }
    
    @RequestMapping(value = "/channel/{id}/edit", method = RequestMethod.GET)
    public String editChannel(@PathVariable("id") long id, Model model, @RequestParam(required=false) String error) {
    	if(error != null && !error.equals("False")) {
        	if(error.equals("1"))
        		model.addAttribute("message_erreur", "La date de fin est antérieure à la date de début");
        	if(error.equals("2"))
        		model.addAttribute("message_erreur", "La date de fin est antérieure à la date actuelle");
        	if(error.equals("3"))
        		model.addAttribute("message_erreur", "Erreur dans la saisie des dates");
    	}
    	Channel channel = channelRepository.getById(id);
    	model.addAttribute(channel);
    	return "edit_channel";
    }
    
    @RequestMapping(value = "/channel/{id}/update", method = RequestMethod.POST)
    public String updateChannel(@PathVariable("id") long id, @ModelAttribute Channel channel) {
    	   	
    	SimpleDateFormat formatter5=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");  
    	Date dateS = null;
    	Date dateE = null;
		try {
			dateS = formatter5.parse(channel.getStartDate());
			dateE=formatter5.parse(channel.getEndDate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "redirect:/channel/"+id+"/edit?error=3";
		}
    	
    	if(dateS.after(dateE))
    		return "redirect:/channel/"+id+"/edit?error=1";
    	
    	if(dateE.before(new Date()))
    		return "redirect:/channel/"+id+"/edit?error=2";
    	
    	Channel chan = channelRepository.getById(id);
    	chan.setDescription(channel.getDescription());
    	chan.setEndDate(channel.getEndDate());
    	chan.setStartDate(channel.getStartDate());
    	chan.setStatus(channel.getStatus());
    	chan.setName(channel.getName());
    	
    	channelRepository.save(chan);
    	return "redirect:/channels-owned";
    }
    
    @RequestMapping(value = "/channel/{channel_id}/remove-user/{user_id}", method = RequestMethod.GET)
    public String getSupprimerUtilisateur(@PathVariable("channel_id") long channel_id, @PathVariable("user_id") long user_id,Principal principal) {
    	System.out.println("selected channel "+channel_id+ " user "+user_id);
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	AppUser us = userRepository.findByMail(loginedUser.getUsername());
    	    	
    	Channel channel = channelRepository.getById(channel_id);
    	if(channel.getOwner().getId() != us.getId())
    		return "redirect:/channels-owned?error=1";
    	AppUser user = userRepository.getById(user_id);
    	 	
    	user.getChannels().remove(channel);
    	userRepository.save(user);
    	
    	    	
    	return "redirect:/channel/"+channel_id+"/users";
    }
    
    @RequestMapping(value = "/channel/{channel_id}/add-user", method = RequestMethod.GET)
    public String getAjouterUtilisateur(@PathVariable("channel_id") long channel_id, @ModelAttribute AppUser selected_user, Principal principal) {
    	if(selected_user.getId() != 0) {
    	   	Channel channel = channelRepository.getById(channel_id);
    	   	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
        	AppUser us = userRepository.findByMail(loginedUser.getUsername());
    	   	if(channel.getOwner().getId() != us.getId())
        		return "redirect:/channels-owned?error=1";
    	   	
        	AppUser user = userRepository.getById(selected_user.getId());
        	
        	
        	//channel.getUsers().add(user1);
        	//channelRepository.save(channel);
        	
        	user.getChannels().add(channel);
        	userRepository.save(user);
        	
        	
        	//model.addAttribute("users", channel.getUsers());
        	//model.addAttribute("channel_id", channel_id);
        	
    	}
    	return "redirect:/channel/"+channel_id+"/users";
 
    }
    

}
