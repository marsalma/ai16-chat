package fr.utc.sr03.chat.controller;

import static java.lang.String.format;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.stereotype.Controller;
import fr.utc.sr03.chat.model.ChatMessage;

@Controller
public class WebSocketController {

	private static final Logger logger = LoggerFactory.getLogger(WebSocketController.class);
    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

	@MessageMapping("/chat/{room_id}/sendMessage")
   // @SendTo("/topic/{id}")
    public void sendMessage(@DestinationVariable("room_id") String room_id, @Payload ChatMessage chatMessage) {
    	System.out.println("WebSocketController sendMessage");
    	 messagingTemplate.convertAndSend(format("/chat-room/%s", room_id), chatMessage);
    }
	
	@Autowired
	private SimpUserRegistry simpUserRegistry;
	
	//@GetMapping("/chat/{room_id}/users")
    public List<String> connectedUsers() {
        return this.simpUserRegistry
                .getUsers()
                .stream()
                .map(SimpUser::getName)
                .collect(Collectors.toList());
    }
    
  //@GetMapping("/chat/{room_id}/users")
    public String getConnectedUsers() {
    	String list = "";
        for(String nom : connectedUsers()) {
        	list += nom +";";
        }
        System.out.println("Liste utilisateurs : "+list);
        return list;
    }
	
	public void sendConnectedUsersList(ChatMessage chatMessage, String currentRoomId) {
		ChatMessage listMessage = new ChatMessage();
		listMessage.setType(ChatMessage.MessageType.USERS);
		listMessage.setSender(chatMessage.getSender());
		listMessage.setContent(getConnectedUsers());
        messagingTemplate.convertAndSend(format("/chat-room/%s", currentRoomId), listMessage);
	}

    @MessageMapping("/chat/{room_id}/addUser")
    //@SendTo("/topic/{id}")
    public void addUser(@DestinationVariable("room_id") String roomId, @Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
    	System.out.println("WebSocketController addUser");
    	String currentRoomId = (String) headerAccessor.getSessionAttributes().put("room_id", roomId);
        // Add username in web socket session
        
        if (currentRoomId != null) {
            ChatMessage leaveMessage = new ChatMessage();
            leaveMessage.setType(ChatMessage.MessageType.LEAVE);
            leaveMessage.setSender(chatMessage.getSender());
            messagingTemplate.convertAndSend(format("/chat-room/%s", currentRoomId), leaveMessage);
        }
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        sendConnectedUsersList(chatMessage, roomId) ;
        messagingTemplate.convertAndSend(format("/chat-room/%s", roomId), chatMessage);
    }

}