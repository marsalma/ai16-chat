package fr.utc.sr03.chat.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.utc.sr03.chat.dao.AppUserRepository;
import fr.utc.sr03.chat.details.AppUserDetails;
import fr.utc.sr03.chat.model.AppUser;
import fr.utc.sr03.chat.utils.EncrytedPasswordUtils;
import fr.utc.sr03.chat.utils.WebUtils;
import fr.utc.sr03.chat.snippets.SendMail;


@Controller
public class UserController {

	@Autowired
    private AppUserRepository userRepository;

    @Autowired
    private SendMail sendMail;
	
	@RequestMapping(value = "/user/info", method = RequestMethod.GET)
    public String userInfo(Model model, Principal principal) {

        // (1) (en)
        // After user login successfully.
        String userName = principal.getName();

        System.out.println("User Name: " + userName);

        AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();

        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);

        return "user_info";
    }
	
	@RequestMapping(value = "/user/changePassword", method = RequestMethod.GET)
    public String getChangePassword(Model model, Principal principal) {
        AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();

        AppUser user = userRepository.findByMail(loginedUser.getUsername());
        model.addAttribute("user", user);

        return "edit_user_password";
    }
	
	@RequestMapping(value = "/user/updatePassword", method = RequestMethod.POST)
    public String updateChangePassword(@ModelAttribute AppUser userUp, Principal principal) {
        AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();

        AppUser user = userRepository.findByMail(loginedUser.getUsername());
        
        String passwordEncode = EncrytedPasswordUtils.encrytePassword(userUp.getPassword());
    	user.setPassword(passwordEncode);
    	userRepository.save(user);

    	return "redirect:/user/info";
    }
	
	@RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accessDenied(Model model, Principal principal) {

        if (principal != null) {
        	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();

            String userInfo = WebUtils.toString(loginedUser);

            model.addAttribute("userInfo", userInfo);

            String message = "Bonjour " + principal.getName() //
                    + "<br> Vous n'avez pas les permissions nécessaires pour accéder à cette page !";
            model.addAttribute("message", message);

        }

        return "403_page";
    }
	
    @RequestMapping(value = "/signin", method = RequestMethod.GET)
    public String getSignin(Model model) {
        model.addAttribute("user", new AppUser());
        return "signin";
    }
    
    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public String postSignin(@ModelAttribute AppUser user, Model model) {
    	String mail = user.getMail();
    	String passwordEncode = EncrytedPasswordUtils.encrytePassword(user.getPassword());
    	
        AppUser appUser = userRepository.findByMail(mail);
        if (appUser != null) {
        	 return "redirect:/signin?error=true";
        }
        sendMail.sendMail(user);
        user.setPassword(passwordEncode);
        user.setAdmin(false);
        user.setEnabled(true);
        user.setFailedAttempt(0);
        
		userRepository.save(user);
		
        return "login";
    }
}
