package fr.utc.sr03.chat.controller;

import fr.utc.sr03.chat.model.AppUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("login")
public class LoginController {

    @GetMapping
    public String getLogin(Model model) {
        model.addAttribute("user", new AppUser());
        return "login";
    }

}
