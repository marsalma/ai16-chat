package fr.utc.sr03.chat.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WelcomeController {
	
    @RequestMapping(value = { "/", "/welcome_page" }, method = RequestMethod.GET)
    public String welcomePage(Model model) {
        model.addAttribute("title", "Bienvenue");
        model.addAttribute("message", "Bienvenue dans le chat de l'UV AI16 !");
        return "welcome_page";
    }

}
