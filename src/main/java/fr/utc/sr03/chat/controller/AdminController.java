package fr.utc.sr03.chat.controller;

import fr.utc.sr03.chat.snippets.SendMail;

import fr.utc.sr03.chat.dao.AppUserRepository;
import fr.utc.sr03.chat.dao.ChannelRepository;
import fr.utc.sr03.chat.details.AppUserDetails;
import fr.utc.sr03.chat.model.AppUser;
import fr.utc.sr03.chat.model.Channel;
import fr.utc.sr03.chat.utils.EncrytedPasswordUtils;
import fr.utc.sr03.chat.utils.WebUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

@Controller
//@RequestMapping("admin")
public class AdminController {
    @Autowired
    private AppUserRepository userRepository;
    @Autowired
    private ChannelRepository channelRepository;
	@Autowired
    private SendMail sendMail;

    @GetMapping("admin/users")
    public String getUserList(Model model, @RequestParam(required=false) String error) {
    	System.out.println("AdminController, getUserList");
        List<AppUser> users = userRepository.findAll();
        model.addAttribute("users", users);
        
        if(error != null && !error.equals("False")) {
        	if(error.equals("supp"))
        		model.addAttribute("message_erreur", "Impossible de se supprimer soi même");
        	if(error.equals("des"))
        		model.addAttribute("message_erreur", "Impossible de se désactiver soi même");
       	
        }
        return "user_list";
    }
    @GetMapping("admin/blocked-users")
    public String getBlockedUserList(Model model) {
        List<AppUser> users = userRepository.findAllByLocked(true);
        model.addAttribute("users", users);

        return "blocked_user_list";
    }
    
    @GetMapping("admin/disabled-users")
    public String getDisabledUserList(Model model) {
        List<AppUser> users = userRepository.findAllByEnabled(false);
        model.addAttribute("users", users);

        return "disabled_user_list";
    }
    
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(Model model, Principal principal) {
        
        AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();

        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);
        
        return "admin_page";
    }
    
    @RequestMapping(value = "/admin/create-user", method = RequestMethod.GET)
    public String getCreateUser(Model model) {
        model.addAttribute("user", new AppUser());
        return "create_user";
    }
    
    @RequestMapping(value = "/admin/create-user", method = RequestMethod.POST)
    public String postCreateUser(@ModelAttribute AppUser user, Model model) {
    	String mail = user.getMail();
    	String passwordEncode = EncrytedPasswordUtils.encrytePassword(user.getPassword());
    	
        AppUser appUser = userRepository.findByMail(mail);
        if (appUser != null) {
        	 return "redirect:/admin/create-user?error=true";
        }
		
        sendMail.sendMail(user);
        user.setPassword(passwordEncode);
        user.setAdmin(false);
        user.setEnabled(true);
        user.setFailedAttempt(0);
        
		userRepository.save(user);
		
        return "admin_page";
    }
    
    @RequestMapping(value = "/admin/activate/{id}", method = RequestMethod.GET)
    public String activateUser(@PathVariable("id") long id, Model model) {
    	System.out.println( "Activation de l'utilisateur "+id);
    	
    	AppUser user = userRepository.getById(id);
    	
    	user.setEnabled(true);
    	
    	userRepository.save(user);
    	
    	return "redirect:/admin/users";
    }
    
    @RequestMapping(value = "/admin/reactivate/{id}", method = RequestMethod.GET)
    public String reactivateUser(@PathVariable("id") long id, Model model) {
    	System.out.println( "Activation de l'utilisateur "+id);
    	
    	AppUser user = userRepository.getById(id);
    	
    	user.setEnabled(true);
   	
    	userRepository.save(user);
    	
    	return "redirect:/admin/disabled-users";
    }
    
    @RequestMapping(value = "/admin/desactivate/{id}", method = RequestMethod.GET)
    public String desactivateUser(@PathVariable("id") long id, Model model, Principal principal) {
    	System.out.println( "Desactivation de l'utilisateur "+id);
    	
    	AppUser user = userRepository.getById(id);
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	if(user.getMail().equals(loginedUser.getUsername()))
    		return "redirect:/admin/users?error=des";
    	
    	user.setEnabled(false);
   	
    	userRepository.save(user);
    	
    	return "redirect:/admin/users";
    }
    
    @RequestMapping(value = "/admin/blocked-users/unblock/{id}", method = RequestMethod.GET)
    public String unblockUserInList(@PathVariable("id") long id, Model model) {
    	System.out.println( "Déblocage de l'utilisateur "+id);
    	
    	AppUser user = userRepository.getById(id);
    	
    	user.setLocked(false);
    	user.setLockTime(null);
    	
    	userRepository.save(user);
    	
    	return "redirect:/admin/blocked-users";
    }  
    
    @RequestMapping(value = "/admin/unblock/{id}", method = RequestMethod.GET)
    public String unblockUser(@PathVariable("id") long id, Model model) {
    	System.out.println( "Déblocage de l'utilisateur "+id);
    	
    	AppUser user = userRepository.getById(id);
    	
    	user.setLocked(false);
    	user.setLockTime(null);
    	
    	userRepository.save(user);
    	
    	return "redirect:/admin/users";
    }  
    
    @RequestMapping(value = "/admin/block/{id}", method = RequestMethod.GET)
    public String blockUser(@PathVariable("id") long id, Model model, Principal principal) {
    	System.out.println( "Blocage de l'utilisateur "+id);
    	
    	AppUser user = userRepository.getById(id);
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	if(user.getMail().equals(loginedUser.getUsername()))
    		return "redirect:/admin/users?error=des";
    	
    	user.setLocked(true);
    	user.setLockTime(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
    	
    	userRepository.save(user);
    	
    	return "redirect:/admin/blocked-users";
    }
    
    @RequestMapping(value = "/admin/delete/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable("id") long id, Model model, Principal principal) {
    	System.out.println( "Suppression de l'utilisateur "+id);
    	
    	AppUser user = userRepository.getById(id);
    	AppUserDetails loginedUser = (AppUserDetails) ((Authentication) principal).getPrincipal();
    	
    	if(user.getMail().equals(loginedUser.getUsername()))
    		return "redirect:/admin/users?error=supp";
    	
    	// on vide la liste des channels dans lequel est l'utilisateur
    	user.getChannels().clear();
    	
    	// on supprime les channels dans lequels est l'utilisateur 
    	Set<Channel> channels = user.getChannelsOwned();
    	for(Channel channel : channels) {
    		// pour supprimer le channel, on supprime les utilisateurs du channel
        	Set<AppUser> users = channel.getUsers();
        	for(AppUser us : users) {
        		us.getChannels().remove(channel);
        		userRepository.save(us);
        	}
        	channelRepository.delete(channel);
    	}
    	userRepository.save(user);
    	userRepository.delete(user);
    	
    	return "redirect:/admin/users";
    }
    
    @RequestMapping(value = "/admin/edit/{id}", method = RequestMethod.GET)
    public String editUser(@PathVariable("id") long id, Model model) {
    	System.out.println( "Edition de l'utilisateur "+id);
   	
    	AppUser user = userRepository.getById(id);
    	System.out.print(user.getMail());
    	model.addAttribute("user", user);
    	
    	return "edit_user";
    }
    
    @RequestMapping(value = "/admin/edit/update-user", method = RequestMethod.POST)
    public String updateUser(@ModelAttribute AppUser userUp, Model model) {
    	System.out.println( "Mise à jour de l'utilisateur ");
    	AppUser user = userRepository.getById(userUp.getId());
    	user.setFirstName(userUp.getFirstName());
    	user.setLastName(userUp.getLastName());
    	user.setMail(userUp.getMail());
    	String passwordEncode = EncrytedPasswordUtils.encrytePassword(userUp.getPassword());
    	user.setPassword(passwordEncode);
    	user.setEnabled(userUp.isEnabled());
    	user.setAdmin(userUp.isAdmin());
    	
    	if(userUp.isEnabled() != true) {
    		System.out.println(userUp.isEnabled());
    		user.setEnabled(false);
    	}
    		
    	userRepository.save(user);
    	
    	return "redirect:/admin/users";
    }
    

}
