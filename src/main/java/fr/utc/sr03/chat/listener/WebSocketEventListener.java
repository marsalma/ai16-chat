package fr.utc.sr03.chat.listener;

import static java.lang.String.format;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import fr.utc.sr03.chat.model.ChatMessage;

@Component
public class WebSocketEventListener {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;
	@Autowired
	private SimpUserRegistry simpUserRegistry;
	

    public List<String> connectedUsers() {
        return this.simpUserRegistry
                .getUsers()
                .stream()
                .map(SimpUser::getName)
                .collect(Collectors.toList());
    }
    
    public String getConnectedUsers() {
    	String list = "";
        for(String nom : connectedUsers()) {
        	list += nom +";";
        }
        System.out.println("Liste utilisateurs : "+list);
        return list;
    }
    
	public void sendConnectedUsersList(String mail, String currentRoomId) {
		ChatMessage listMessage = new ChatMessage();
		listMessage.setType(ChatMessage.MessageType.USERS);
		listMessage.setSender(mail);
		listMessage.setContent(getConnectedUsers());
        messagingTemplate.convertAndSend(format("/chat-room/%s", currentRoomId), listMessage);
	}
	
    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        logger.info("Received a new web socket connection");
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

        String mail = (String) headerAccessor.getSessionAttributes().get("username");
        
        String room_id = (String) headerAccessor.getSessionAttributes().get("room_id");
        
        if(mail != null) {
            logger.info("User Disconnected : " + mail);
            sendConnectedUsersList(mail, room_id) ;
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setType(ChatMessage.MessageType.LEAVE);
            chatMessage.setSender(mail);

            messagingTemplate.convertAndSend(format("/chat-room/%s", room_id), chatMessage);
        }
    }
    
}