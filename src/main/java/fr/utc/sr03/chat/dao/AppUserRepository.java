package fr.utc.sr03.chat.dao;

import fr.utc.sr03.chat.model.AppUser;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    AppUser findByMailAndPassword(@Param("mail") String mail, @Param("password") String password);
    AppUser findByMailAndPasswordAndAdmin(@Param("mail") String mail, @Param("password") String password, @Param("admin") boolean admin);
	AppUser findByMail(@Param("mail") String mail);
	List<AppUser> findAllByEnabled(@Param("enabled") boolean enabled);
	List<AppUser> findAllByLocked(@Param("locked") boolean locked);
	
	@Query("UPDATE AppUser u SET u.failedAttempt = ?1 WHERE u.mail = ?2")
    @Modifying
    public void updateFailedAttempts(int failAttempts, String email);

	@Query("SELECT u from AppUser u WHERE NOT EXISTS (SELECT c FROM u.channels c where c.id = ?1)")
    public List<AppUser> findUsersNotInChannel(Long id);
	
	@Query("SELECT COUNT(u) FROM AppUser u JOIN u.channels c where c.id = ?1")
    public int countUsersInChannel(Long id);
}
