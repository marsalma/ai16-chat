package fr.utc.sr03.chat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import fr.utc.sr03.chat.model.Channel;

public interface ChannelRepository extends JpaRepository<Channel, Long> {

	Channel findByName(@Param("name") String name);

}
