'use strict';


var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('#connecting');

var room_id = document.getElementById('id').value;
var username = document.getElementById('username').value;

var date_end = document.getElementById('date_end').value;

var stompClient = null;
var topic = null;
var currentSubscription;

function endChat(){
	alert('Fin du chat !');
	stompClient.disconnect();
	document.location.href = '/channels';
}


function checkTime(){

    const iso_date_end = new Date(date_end).toISOString();
	const parse_iso_date_end = Date.parse(iso_date_end);
		
	const date_now = new Date().toISOString();
	const parse_date_now = Date.parse(date_now);
	
	const chat_time = parse_iso_date_end - parse_date_now;
	// si le temps dépasse la valeur maximum, cela plante
	if(chat_time > 2147483647)
		chat_time = 2147483647;
	console.log(chat_time);
	setTimeout(endChat, chat_time); 
}
 

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, onConnected, onError);
}

// Connect to WebSocket Server.
connect();
checkTime(); 

function onConnected() {
	enterRoom();
}

function enterRoom() {
	//Cookies.set('room_id', room); // vraiment nécessaire ?
	topic = `/chat-app/chat/${room_id}`;
  
    // Subscribe to the channel
	currentSubscription = stompClient.subscribe(`/chat-room/${room_id}`, onMessageReceived);
	
	// Tell your username to the server
	stompClient.send(`${topic}/addUser`,
    	{},
    	JSON.stringify({sender: username, type: 'JOIN'})
	);

    connectingElement.classList.add('hidden');
}

function onError(error) {
    connectingElement.textContent = 'Impossible de se connecter au WebSocket server. Essayer d\'actualiser la page avant de réessayer !';
    connectingElement.style.color = 'red';
}


function sendMessage(event) {
    
    var messageContent = messageInput.value.trim();
    topic = `/chat-app/chat/${room_id}`;
    if(messageContent && stompClient) {
        var chatMessage = {
            sender: username,
            content: messageContent,
            type: 'CHAT'
        };

        stompClient.send(`${topic}/sendMessage`, {}, JSON.stringify(chatMessage));
        document.querySelector('#message').value = '';
    }
    event.preventDefault();
}


function onMessageReceived(payload) {
	console.log("onMessageReceived");
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');

	if (message.type === 'USERS') {
		let text = message.content;
		console.log("text "+text);
		var users = text.split(";");
		users.pop();
		console.log(users);
		var ul = document.createElement("ul");
		ul.classList.add("list-group");
		
		for (var i = 0; i < users.length; i++) {
		   var userElem = document.createTextNode(users[i]);
		   var li = document.createElement("li");
		   li.appendChild(userElem);
		   li.classList.add("list-group-item");
		   ul.appendChild(li);
		}
		
		var connectedUsers = document.querySelector('#connected-users') //on ajoute la liste
        connectedUsers.replaceChildren(ul);
    } else {
	    if(message.type === 'JOIN') {
	        messageElement.classList.add('event-message');
	        message.content = message.sender + ' a rejoint le chat !';
	    } else if (message.type === 'LEAVE') {
	        messageElement.classList.add('event-message');
	        message.content = message.sender + ' a quitté le chat !';
	    } else  {
	        messageElement.classList.add('chat-message');   
	        var usernameElement = document.createElement('strong');
	        usernameElement.classList.add('nickname');
	        var usernameText = document.createTextNode(message.sender);
	        var usernameText = document.createTextNode(message.sender);
	        usernameElement.appendChild(usernameText);
	        messageElement.appendChild(usernameElement);
	    }
	
	    var textElement = document.createElement('span');
	    var messageText = document.createTextNode(message.content);
	    textElement.appendChild(messageText);
	
	    messageElement.appendChild(textElement);
	
	    messageArea.appendChild(messageElement);
	    messageArea.scrollTop = messageArea.scrollHeight;
    }
}


 
messageForm.addEventListener('submit', sendMessage, true);